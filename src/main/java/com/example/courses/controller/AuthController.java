package com.example.courses.controller;

import com.example.courses.model.AuthRequest;
import com.example.courses.repository.UserRepository;
import com.example.courses.security.JwtTokenUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.courses.model.GeneralResponse;

import java.util.HashMap;

@RestController
@RequestMapping("/v1/login/*")
@RequiredArgsConstructor
public class AuthController {
    private final UserRepository userRepository;
    private AuthenticationManager authenticationManager;
    private final JwtTokenUtil jwtUtil;

    @PostMapping(value = "/authenticate")
    public ResponseEntity<GeneralResponse> sendVerificationCode(@RequestBody AuthRequest authRequest) {
        GeneralResponse response = new GeneralResponse();
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword()));
        if (authentication.isAuthenticated()){
            String token = jwtUtil.createToken(new HashMap<>(), authRequest.getUsername());
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("token", token);
            return ResponseEntity.ok()
                    .headers(responseHeaders)
                    .body(response);
        }else {
            response.setFault("Authentication failed");
            return ResponseEntity.badRequest()
                    .body(response);
        }
    }
}
