package com.example.courses.controller;

import com.example.courses.constants.MDCData;
import com.example.courses.entity.Course;
import com.example.courses.repository.CourseRepository;
import com.example.courses.repository.UserRepository;
import com.example.courses.utils.MdcServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashSet;

@RestController
@RequiredArgsConstructor
public class CourseController {
    private final CourseRepository courseRepository;
    private final MdcServiceImpl mdcService;
    private final UserRepository userRepository;

    @GetMapping("/courses")
    public ResponseEntity<Iterable<Course>> getCourses() {
        return new ResponseEntity<>(courseRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/courses/{id}")
    public ResponseEntity<Course> getCourse(@PathVariable Long id) {
        return new ResponseEntity<>(courseRepository.findById(id).orElseThrow(), HttpStatus.OK);
    }

    @PostMapping("/courses/{id}/register")
    public ResponseEntity<String> registerForCourse(@PathVariable Long id, @RequestBody String username) {
        Course course = courseRepository.findById(id).orElseThrow();
        if (course.getUsers()!=null){

            course.getUsers().add(userRepository.findByUsername(mdcService.get(MDCData.TOKEN_USER_NAME.name())));
        }else {
            course.setUsers(new HashSet<>());
            course.getUsers().add(userRepository.findByUsername(mdcService.get(MDCData.TOKEN_USER_NAME.name())));
        }

        return new ResponseEntity<>("Registered successfully", HttpStatus.OK);
    }

    @PostMapping("/courses/{id}/cancel")
    public ResponseEntity<String> cancelCourseRegistration(@PathVariable Long id, @RequestBody String username) {
        Course course = courseRepository.findById(id).orElseThrow();
        if (course.getUsers()!=null){
            course.getUsers().remove(userRepository.findByUsername(mdcService.get(MDCData.TOKEN_USER_NAME.name())));
        }
        return new ResponseEntity<>("Cancelled successfully", HttpStatus.OK);
    }
}
