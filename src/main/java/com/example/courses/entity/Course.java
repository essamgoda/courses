package com.example.courses.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.Set;

@Entity
@Table(name = "COURSES")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long CourseId;
    private String name;
    private String description;

    @ManyToMany
    Set<SystemUser> users;

    // getters and setters
}