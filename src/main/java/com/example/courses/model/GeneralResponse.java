package com.example.courses.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import com.example.courses.model.exception.BusinessException;

@Setter
@Getter
public class GeneralResponse implements Serializable {
    private String message = "Success";
    private String timestamp;
    private String status ="Success";


    public GeneralResponse() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
        Timestamp date=new Timestamp(System.currentTimeMillis());
        timestamp= dateFormat.format(date);
    }
    public void setFault(BusinessException e){
        this.message = e.getUserMessage();
        this.status = "Fail";

    }

    public void setFault(String message) {
        this.message = message;
        this.status = "Fail";
    }
}
