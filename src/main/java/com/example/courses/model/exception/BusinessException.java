package com.example.courses.model.exception;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Setter
@Getter
public class BusinessException extends RuntimeException {
    private String errorCode;
    private String userMessage;

    public BusinessException( String errorCode, String userMessage,String message) {
        super(message);
        this.errorCode = errorCode;
        this.userMessage = userMessage;
    }

    public BusinessException( String errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }
    public BusinessException(String errorCode) {
        this.errorCode = errorCode;
    }
}
