package com.example.courses.repository;

import com.example.courses.entity.SystemUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<SystemUser, Long> {
    SystemUser findByUsername(final String username);
}

