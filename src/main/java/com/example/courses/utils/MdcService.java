package com.example.courses.utils;

public interface MdcService {
    void put(String key, String value);

    void remove(String key);

    String get(String key);

}
