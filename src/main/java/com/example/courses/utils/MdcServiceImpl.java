package com.example.courses.utils;

import org.slf4j.MDC;
import org.springframework.stereotype.Component;

@Component
public class MdcServiceImpl implements MdcService {
    @Override
    public void put(String key, String value) {
        MDC.put(key, value);
    }

    @Override
    public void remove(String key) {
        MDC.remove(key);
    }

    @Override
    public String get(String key) {
        return MDC.get(key);
    }
}
